from keras.applications.imagenet_utils import preprocess_input, decode_predictions
from keras.preprocessing import image
from keras.applications.mobilenet import MobileNet
import numpy as np

model = MobileNet(weights='imagenet')

def model_predict(img_path, model):
	# Load the image
	img = image.load_img(img_path, target_size=(224,224))

	# Preprocess the image
	x = image.img_to_array(img)
	x = np.expand_dims(x, axis=0)
	x = preprocess_input(x, mode='caffe')

	prediction = model.predict(x)
	return prediction

def upload_file(file_path):
	preds = model_predict(file_path, model)
	# Decode the predictions to a string format
	pred_class = decode_predictions(preds, top=1)
	result = str(pred_class[0][0][1])
	print(result)

if __name__ == '__main__':
	file_path = input('Where is your image: ')
	upload_file(file_path)
